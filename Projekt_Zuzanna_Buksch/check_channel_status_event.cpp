#include "check_channel_status_event.h"

CheckChannelStatusEvent::CheckChannelStatusEvent(size_t time, WirelessNetwork* wireless_network, EventList* event_list, Logger* logger_ptr, int id)
  :Event(time, wireless_network), event_list_(event_list), logger_ptr_(logger_ptr), transmitter_id(id)
{
}

CheckChannelStatusEvent::~CheckChannelStatusEvent()
{
}



void CheckChannelStatusEvent::Execute()
{
  if (!wireless_network_->GetTransmitters()->at(transmitter_id)->GetPackets()->empty())
  { 
    static size_t time_of_start_transmission = 0; // zmienna pomocnicza do sprawdzania czy inny nadajnik rozpocz�� transmisje w tym samym czasie
    wireless_network_->GetTransmitters()->at(transmitter_id)->SetCheckStatus(true); // nadajnik rozpocz�� ju� nas�uchiwanie

    if (wireless_network_->GetChannel()->GetChannelStatus() == true) // je�li kana� jest wolny
    {
      //logger_ptr_->Info("Channel is idle");

      if (wireless_network_->GetTransmitters()->at(transmitter_id)->GetWaitingTime() > wireless_network_->kDifsTime) // je�li kana� jest wolny dlu�ej ni� 5ms -> 50
      {
        time_of_start_transmission = time_;
        wireless_network_->GetChannel()->SetChannelStatus(false);
        wireless_network_->GetTransmitters()->at(transmitter_id)->SetWaitingTime(0); // ustaw czas nas�uchiwania na 0
        auto packet_ptr = wireless_network_->GetTransmitters()->at(transmitter_id)->GetPackets()->front();
        wireless_network_->GetTransmitters()->at(transmitter_id)->StartTransmission(logger_ptr_, packet_ptr, wireless_network_, time_); // rozpocznij transmisje pakietu    
        size_t transmission_time = wireless_network_->GetTransmitters()->at(transmitter_id)->GetPackets()->front()->GetCtpkTime();
        event_list_->push(new EndTransmissionPacketEvent(time_ + transmission_time, wireless_network_, event_list_, logger_ptr_, transmitter_id));
       }

      else // je�li kana� jest wolny kr�cej ni� 5 ms -> 50
      {
        //logger_ptr_->Info("Transmitter "+std::to_string(transmitter_id)+", waiting time: " + std::to_string(wireless_network_->GetTransmitters()->at(transmitter_id)->GetWaitingTime()));
        size_t temp = wireless_network_->GetTransmitters()->at(transmitter_id)->GetWaitingTime() + (wireless_network_->kWaitTime);
        wireless_network_->GetTransmitters()->at(transmitter_id)->SetWaitingTime(temp); // zwi�ksz czas na�uchiwania o 0.5 ms -> 5
        event_list_->push(new CheckChannelStatusEvent(time_ + (wireless_network_->kWaitTime), wireless_network_, event_list_, logger_ptr_, transmitter_id));
      }
    }

    else // je�li kana� nie jest wolny 
    {
      if (wireless_network_->GetTransmitters()->at(transmitter_id)->GetWaitingTime() > wireless_network_-> kDifsTime && time_of_start_transmission == time_) // je�li pakiet nas�uchiwa� ju� > difs
      {
        wireless_network_->GetTransmitters()->at(transmitter_id)->SetWaitingTime(0); // ustaw czas nas�uchiwania na 0
        auto packet_ptr = wireless_network_->GetTransmitters()->at(transmitter_id)->GetPackets()->front();
        wireless_network_->GetTransmitters()->at(transmitter_id)->StartTransmission(logger_ptr_, packet_ptr, wireless_network_, time_); // rozpocznij transmisje pakietu    
        size_t transmission_time = wireless_network_->GetTransmitters()->at(transmitter_id)->GetPackets()->front()->GetCtpkTime();
        event_list_->push(new EndTransmissionPacketEvent(time_ + transmission_time, wireless_network_, event_list_, logger_ptr_, transmitter_id));
      }
      else
      {
        //logger_ptr_->Info("Channel is busy...");
        wireless_network_->GetTransmitters()->at(transmitter_id)->SetWaitingTime(0);
        event_list_->push(new CheckChannelStatusEvent(time_ + (wireless_network_->kWaitTime), wireless_network_, event_list_, logger_ptr_, transmitter_id));
      }
    }
  }
  else
  {
    wireless_network_->GetTransmitters()->at(transmitter_id)->SetCheckStatus(false);
  } 
}
